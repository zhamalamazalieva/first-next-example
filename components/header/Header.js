import React from "react";
import {Col, Row } from "react-bootstrap"
import Link from "next/link";
import styles from "../../styles/Header.module.css"


const Header = () => {
	return(
		<Row className={`${styles.header} d-flex  py-3`}>
			<Col className="">
				<Link href="/" className={`${styles.link}`}>Home Page</Link>
			</Col>
			<Col>
				<Link href="/users"  className={`${styles.link}`}>Users</Link>
			</Col>
			<Col>
				<Link href="/posts/posts"  className={`${styles.link}`}>Posts</Link>
			</Col>
		</Row>
	)
}
export default Header