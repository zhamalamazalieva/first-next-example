import MainContainer from "../../components/main-container/MainContainer";
import React, { useState} from "react";
import {Col, Row } from "react-bootstrap"
import { useRouter} from "next/router";


const User = ({ user }) => {
	return(
		<MainContainer>
			<Row className="d-flex flex-column">
				{user.name}
			</Row>

		</MainContainer>
	)
}
export default User

export async function getServerSideProps({ params }){
	console.log(params)
	const res = await fetch(`https://jsonplaceholder.typicode.com/users/${params.id}`)
	const user = await res.json()
	return {
		props: {
			user
		}
	}
}
