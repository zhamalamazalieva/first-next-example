import MainContainer from "../../components/main-container/MainContainer";
import React, { useState} from "react";
import {Col, Row } from "react-bootstrap"
import Link from "next/link";



const Index = ({ users }) => {
	return(
		<MainContainer>
	<Row className="d-flex flex-column">
		{users.map(item =>
				<Col key={item.id}>
<Link href={`/users/${item.id}`}>{item.name}</Link>
				</Col>)}
	</Row>

		</MainContainer>
	)
}
export default Index

export async function getStaticProps(context){
	const res = await fetch("https://jsonplaceholder.typicode.com/users")
	const users = await res.json()
	return {
		props: {
			users
		}
	}
}